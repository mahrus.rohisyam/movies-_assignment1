// import 'react-native-gesture-handler';
// import * as React from 'react';
// import { NavigationContainer } from '@react-navigation/native';
// import HomeScreen from '../src/activity/HomeScreen';
// import SignUp from '../src/activity/SignUp';

// const Navigasi = () => {
//   return (
//     <NavigationContainer>
//       <Stack.Navigator initialRouteName="Login">
//         <Stack.Screen
//           name="Login"              
//           component={HomeScreen}
//           options={hide}
//         />
//         <Stack.Screen name="Signup" component={SignUp} options={hide} />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// };

// export default Navigasi;

import * as React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import DashBoard from '../src/activity/DashBoard';
import HomeScreen from '../src/activity/HomeScreen';
import SignUp from '../src/activity/SignUp';
import MainMenu from '../src/activity/MainMenu';
import FilmList from '../src/activity/FilmList';

const Stack = createStackNavigator();

const MyStack = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator>
        <Stack.Screen
          name="Home"
          component={HomeScreen}
          options={{headerShown:false}}
          />
        <Stack.Screen 
        name="Dashboard" 
        component={DashBoard}
        options={{headerShown:false}}
        />
        <Stack.Screen 
        name="SignUp" 
        component={SignUp}
        options={{headerShown:false}}
        />
        <Stack.Screen
        name="MainMenu"
        component={MainMenu}
        options={{headerShown:true}}
        />
        <Stack.Screen
        name="FilmList"
        component={FilmList}
        options={{headerShown:false}}
        />

      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default MyStack