import React, { Component } from 'react'
import { Text, TouchableOpacity, View,ScrollView,Image,StyleSheet } from 'react-native'
import UHeader1 from '../component/NHeader1'

export default class FilmList extends Component {
    constructor(){
        super()
        this.state={
            title:'Avengers',
            data :[]
        }
    }
    componentDidMount=()=>{
        fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
        .then(response => response.json())
        .then(json => this.setState({data:json.Search}))
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
             // ADD THIS THROW error
              throw error;
            });
    }
    
    render() {
        return (
            <View style={{flex:1}}>
                <UHeader1 title='Daftar film'/>
                <ScrollView style={{backgroundColor:'#1e1e1e'}}>
                {this.state.data.map((value, indeks)=>{
                    return(
                    <View style={{width:'100%',height:125,backgroundColor:'#4d4d4d',flexDirection:'row',alignItems:'center',margin:5,borderRadius:15}} key={indeks}>
                    <View style={{width:80,height:110,margin:10,borderRadius:15,justifyContent:'center',alignItems:'center'}}>
                        <Image source={{uri:value.Poster}} style={{width:60,height:90}}/>
                    </View>
                    <View style={{width:360,height:75,}}>
                        <Text style={styles.text}>{value.Title}</Text>
                        <Text style={styles.text}>{value.Year}</Text>
                        <Text style={styles.text}>{value.imdbID}</Text>
                        <Text style={styles.text}>{value.Type}</Text>
                    </View>
                    </View>
                    )
                })}
                
                </ScrollView>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    text:{
        color:'#fff'

    }
})