import React, { Component } from 'react'
import { ScrollView, Text, View,StyleSheet,TouchableOpacity,Image,BackHandler,Alert } from 'react-native'
import UInput from '../component/UInput'

export default class HomeScreen extends Component {
    backAction = () => {
        Alert.alert("Hold on!", "Are you sure you want to Exit?", [
          {
            text: "Cancel",
            onPress: () => null,
            style: "cancel"
          },
          { text: "YES", onPress: () => BackHandler.exitApp() }
        ]);
        return true;
      };
      componentDidMount() {
        this.backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          this.backAction
        );
      }
    
      componentWillUnmount() {
        this.backHandler.remove();
      }
    render() {
        return (
            <View style={{flex:1}}>
                <ScrollView style={{backgroundColor:'#1e1e1e',padding:15}}>
                <Image source={require('../assets/media/path847.png')} style={styles.logo}/>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                    <Text style={styles.text}>Login To Start Watching!</Text>
                    <UInput title='Email...'/>
                    <UInput title='Password...'/>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{textAlign:'center',fontSize:9, color:'#fff', marginTop:1}}>Does not have account? </Text>
                        <TouchableOpacity onPress={()=>{this.props.navigation.navigate('SignUp')}}>
                            <Text style={{fontSize:9, color:'#ffae00', textDecorationLine:'underline'}}>Sign Up here</Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{margin:15}}>
                    <TouchableOpacity style={styles.UButton1} onPress={()=>{
                        this.props.navigation.navigate('Dashboard')
                    }} >
                        <Text style={{color:'black',}}>Login</Text>
                    </TouchableOpacity> 
                    </View>                                     
                </View>  
                </ScrollView>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    text:{
        fontSize:18,
        color:'#ffae00',
        margin:10
    },
    logo:{
        width:100,
        height:100,
        margin:25,
        justifyContent:'center',
        alignSelf:'center'
    },
    UButton1:{
        width:200,
        height:40,
        backgroundColor:'#ffae00',
        borderRadius:25,
        justifyContent:'center',
        alignItems:'center',
    },
})