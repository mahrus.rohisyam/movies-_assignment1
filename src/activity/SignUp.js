import React, { Component } from 'react'
import { ScrollView, Text, View,StyleSheet,TouchableOpacity,Image,BackHandler,Alert } from 'react-native'
import UInput from '../component/UInput';
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'
import MI from 'react-native-vector-icons/MaterialIcons'
export default class SignUp extends Component {
    backAction = () => {
    };
    componentDidMount() {
      this.backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        this.backAction
      );
    }
  
    componentWillUnmount() {
      this.backHandler.remove();
    }
    render() {
        return (
          <View style={{flex:1}}>
          <ScrollView style={{backgroundColor:'#1e1e1e',padding:15}}>
          <Image source={require('../assets/media/path847.png')} style={styles.logo}/>
          <View style={{justifyContent:'center',alignItems:'center'}}>
              <Text style={styles.text}>Join Us!<Text style={{fontSize:18,color:'white'}}> to Start Watching</Text></Text>
              <View style={styles.icon} >
              <MCI name='email' size={30} color='#ffae00'/>
              <UInput title='Email...'/>
              </View>
              <View style={styles.icon} >
              <MI name='person' size={30} color='#ffae00'/>
              <UInput title='Username...'/>
              </View>
              <View style={styles.icon} >
              <MCI name='lock' size={30} color='#ffae00'/>
              <UInput title='Password...'/>
              </View>
              <View style={{flexDirection:'row', alignItems:'center'}}>
                  <Text style={{textAlign:'center',fontSize:9, color:'#fff', marginTop:1}}>Have an account? </Text>
                  <TouchableOpacity onPress={()=>{this.props.navigation.navigate('Home')}}>
                      <Text style={{fontSize:9, color:'#ffae00', textDecorationLine:'underline'}}>Login Instead</Text>
                  </TouchableOpacity>
              </View>
              <View style={{margin:15}}>
              <TouchableOpacity style={styles.UButton1} onPress={()=>{
                  this.props.navigation.navigate('Home')
              }} >
                  <Text style={{color:'white',}}>Sign Up</Text>
              </TouchableOpacity> 
              </View>                                     
          </View>  
          </ScrollView>
      </View>
        )
    }
}
const styles=StyleSheet.create({
  text:{
      fontSize:18,
      color:'#ffae00',
      margin:10
  },
  logo:{
      width:100,
      height:100,
      margin:25,
      justifyContent:'center',
      alignSelf:'center'
  },
  UButton1:{
    width:200,
    height:50,
    borderWidth:1.5,
    borderColor:'#ffae00',
    borderRadius:25,
    justifyContent:'center',
    alignItems:'center',
},
  icon:{
      justifyContent:'center',
      alignItems:'center',
      flexDirection:'row'
  }
})