import React, { Component } from 'react'
import { ScrollView, Text, View,StyleSheet, TouchableOpacity,BackHandler } from 'react-native'
import BubbleTick from '../component/BubbleTick'
import MI from 'react-native-vector-icons/MaterialIcons'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'
export default class MainMenu extends Component {
    backAction = () => {
    };
    componentDidMount() {
      this.backHandler = BackHandler.addEventListener(
        "hardwareBackPress",
        this.backAction
      );
    }
  
    componentWillUnmount() {
      this.backHandler.remove();
    }
    render() {
        return (
            <View style={{backgroundColor:'#1e1e1e',flex:1}}>
                <ScrollView>
                <View style={styles.header}>
                    <Text style={styles.textHeader}>Main Menu</Text>
                </View>
                {/* Start Menu */}
                <View style={{marginTop:25,justifyContent:'center',alignItems:'center'}}>
                    <TouchableOpacity><Text style={styles.textMenu}>Dashboard</Text>
                        </TouchableOpacity>
                </View>
                <View style={{justifyContent:'center',alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>{this.props.navigation.navigate('FilmList')}}><Text style={styles.textMenu}>Movie Lists</Text>
                        </TouchableOpacity>
                </View>
                <View style={{justifyContent:'center',alignItems:'center',marginTop:150}}>
                    <Text style={styles.textMenu}>Our Social Media</Text>
                    <View style={{justifyContent:'space-evenly',alignItems:'center',flexDirection:'row'}}>
                        <TouchableOpacity>
                            <MCI name='github' size={50} color='#ffae00'/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <MCI name='facebook' size={50} color='#ffae00'/>
                        </TouchableOpacity>
                        <TouchableOpacity>
                            <MCI name='instagram' size={50} color='#ffae00'/>
                        </TouchableOpacity>
                    </View>
                </View>
                </ScrollView>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    header:{
        width:'100%',
        height:75,
        alignItems:'center',
        flexDirection:'row',
        padding:15,
        elevation:15
    },
    textHeader:{
        fontSize:18,
        color:'#fff',
        paddingLeft:35,
        fontWeight:'bold'
    },
    textMenu:{
        fontSize:18,
        color:'#fff',
        paddingTop:15
    },

})