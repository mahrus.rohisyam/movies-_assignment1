import React, { Component } from 'react'
import { Text, View,BackHandler, ScrollView,StyleSheet,TouchableOpacity, Image,Alert} from 'react-native'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'

export default class DashBoard extends Component {
    backAction = () => {
      };
      componentDidMount() {
        this.backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          this.backAction
        );
        fetch('http://www.omdbapi.com/?s=avengers&apikey=997061b4&')
        .then(response => response.json())
        .then(json => this.setState({data:json.Search}))
        .catch(function(error) {
            console.log('There has been a problem with your fetch operation: ' + error.message);
             // ADD THIS THROW error
              throw error;
            });
      }
    
      componentWillUnmount() {
        this.backHandler.remove();
      }
      constructor(){
        super()
        this.state = {
            title:'Avenger',
            subscribtion:'Regular Member',
            data:[],
        }
      }
    subscribe(){
        this.setState({
            subscribtion:'Star Member'
        })
    }
    Warning = () =>
    Alert.alert(
      "Coming Soon!",
      "Thanks for pressing Contribute button, its let us know that you are love our apps and want contribute.",
      [
        {
          text: "Cancel",
          onPress: () => console.log("Cancel Pressed"),
          style: "cancel"
        },
        { text: "OK", onPress: () => console.log("OK Pressed") }
      ]
    );
    render() {
        return (
            <View style={{flex:1,backgroundColor:'#1e1e1e'}}>
                <View style={styles.header}>
                    <TouchableOpacity oonPress={()=>{this.props.navigation.navigate('Home')}}>
                    <MCI name='format-align-justify' size={30} color='#fff'/>
                    </TouchableOpacity>
                    <Text style={styles.text}>Dashboard</Text>
                </View>
                <ScrollView>
                    <View>
                        <View style={{width:'100%',height:200,backgroundColor:'#ffae00'}}/>
                        <View style={{width:'100%',height:125,justifyContent:'center',alignItems:'center'}}>
                            <Text style={styles.userName}>Jason John Doe</Text>
                            <Text style={{fontSize:12,color:'#ffae00'}}>{this.state.subscribtion}</Text>
                        </View>
                        <View style={{flexDirection:'row',justifyContent:'space-evenly'}}>
                            <TouchableOpacity style={styles.btn} onPress={()=>{this.subscribe()}} >
                                <Text>Upgrade Account</Text>
                            </TouchableOpacity>
                            <TouchableOpacity style={styles.btn} onPress={this.Warning} >
                                <Text>Contribute</Text>
                            </TouchableOpacity>
                        </View>
                        <View style={{justifyContent:'space-evenly'}}>
                            <Text style={styles.NTextO}>0<Text style={styles.NText}> Watch Hours</Text></Text>
                            <View style={{width:'100%',height:1,backgroundColor:'white'}}/>
                        </View>
                        <View style={{ position:'absolute',top: 100, left:100, width:150,borderRadius:100, height: 150, backgroundColor:'#1e1e1e',justifyContent:'center',elevation:15}} >
                        <Image source={require('../assets/media/g4632.png')} style={{position:'absolute', height: 110,width:90,alignSelf:'center'}}/>
                        </View>
                        <Text style={styles.NTextO}>Ops... <Text style={styles.NText}>You have not Watch a Movie</Text></Text>
                        <Text style={styles.NText} >Here are Our Reccomendation</Text>
                    {/* Film List */}
                    {this.state.data.map((value, indeks)=>{
                    return(
                    <View style={{width:'100%',height:125,backgroundColor:'#4d4d4d',flexDirection:'row',alignItems:'center',margin:5,borderRadius:15}} key={indeks}>
                    <View style={{width:80,height:110,margin:10,borderRadius:15,justifyContent:'center',alignItems:'center'}}>
                        <Image source={{uri:value.Poster}} style={{width:60,height:90}}/>
                    </View>
                    <TouchableOpacity>
                        <View style={{width:360,height:75,}}>
                            <Text style={styles.CardText}>{value.Title}</Text>
                            <Text style={styles.CardText}>{value.Year}</Text>
                            <Text style={styles.CardText}>{value.imdbID}</Text>
                            <Text style={styles.CardText}>{value.Type}</Text>
                        </View>
                    </TouchableOpacity>
                    </View>
                    )
                })}
                    </View>

                </ScrollView>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    header:{
        width:'100%',
        height:75,
        justifyContent:'flex-start',
        alignItems:'center',
        flexDirection:'row',
        padding:15,
        elevation:15,
        position:'absolute',
        top:0,
    },
    text:{
        fontSize:18,
        color:'#fff',
        paddingLeft:35
    },
    NText:{
        fontSize:16,
        color:'white',
        padding:5,
        textAlign:'center',
    },
    NTextO:{
        fontSize:20,
        color:'#ffae00',
        padding:10,
        textAlign:'center',
    },
    userName:{
        fontSize:24,
        fontWeight:'bold',
        color:'#fff',
        paddingTop:15,
    },
    btn:{
        backgroundColor:'#ffae00',
        width:150,
        height:50,
        borderRadius:30,
        justifyContent:'center',
        alignItems:'center'
    },
    CardText:{
       color:'white',
    }
})