import React, { Component } from 'react'
import { StyleSheet, Text, View,TextInput } from 'react-native'
import MI from 'react-native-vector-icons/MaterialIcons'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'
export default class UInput extends Component {
    render() {
        return (
            <View style={styles.container}>
                <TextInput style={styles.input} placeholder={this.props.title}/>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    input:{
        borderColor:'#fff',
        backgroundColor:'#fff',
        borderRadius:25,
        width:250,
        height:40,
        paddingLeft:15,
        margin:10,
        color:'#1d1d1d',
    },
})