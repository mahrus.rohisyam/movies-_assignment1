import React, { Component } from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'

export default class UButton2 extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.UButton2}>
                <Text style={styles.text}>{this.props.title}</Text>
            </TouchableOpacity>
        )
    }
}
const styles=StyleSheet.create({
    UButton2:{
        width:200,
        height:50,
        borderWidth:1.5,
        borderColor:'#ffae00',
        borderRadius:25,
        justifyContent:'center',
        alignItems:'center',
    },
    text:{
        fontSize:18,
        color:'white'
    }
})
