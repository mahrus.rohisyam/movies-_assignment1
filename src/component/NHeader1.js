import React, { Component } from 'react'
import { Text, View, StyleSheet,TouchableOpacity } from 'react-native'
import MCI from 'react-native-vector-icons/MaterialCommunityIcons'

export default class NHeader1 extends Component {
    render() {
        return (
            <View style={styles.header}>
                    <TouchableOpacity oonPress={()=>{this.props.navigation.navigate('MainMenu')}}>
                    <MCI name='format-align-justify' size={30} color='#ffae00'/>
                    </TouchableOpacity>
                    <Text style={styles.text}>{this.props.title}</Text>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    header:{
        width:'100%',
        height:75,
        backgroundColor:'#4d4d4d',
        justifyContent:'flex-start',
        alignItems:'center',
        flexDirection:'row',
        padding:15,
        elevation:15
    },
    text:{
        fontSize:18,
        color:'#fff',
        paddingLeft:35
    }
})