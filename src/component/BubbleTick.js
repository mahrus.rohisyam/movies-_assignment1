import React, { Component } from 'react'
import { Text, View,TouchableOpacity,BackHandler, StyleSheet } from 'react-native'

export default class BubbleTick extends Component {
    backAction = () => {
      };
      componentDidMount() {
        this.backHandler = BackHandler.addEventListener(
          "hardwareBackPress",
          this.backAction
        );
        }
    render() {
        return (
                <TouchableOpacity styles={styles.bubble}>
                    <View>
                        <Text> textInComponent </Text>
                    </View>
                </TouchableOpacity>
        )
    }
}
const styles=StyleSheet.create({
    bubble:{
        position:'absolute',
        bottom:1,
        width:100,
        height:100,
        backgroundColor:'black'
    },
    wrapper:{
        width:80,
        height:80,
        backgroundColor:'black',
        
    }
})