import React, { Component } from 'react'
import { StyleSheet, Text, View,TouchableOpacity } from 'react-native'

export default class UButton1 extends Component {
    render() {
        return (
            <TouchableOpacity style={styles.UButton1} onPress={()=>{this.props.navigation.navigate('Login')}}>
                <Text style={styles.text}>{this.props.title}</Text>
            </TouchableOpacity>

        )
    }
}
const styles=StyleSheet.create({
    UButton1:{
        width:200,
        height:40,
        backgroundColor:'#ffae00',
        borderRadius:25,
        justifyContent:'center',
        alignItems:'center',
    },
    text:{
        fontSize:18,
    }
})
