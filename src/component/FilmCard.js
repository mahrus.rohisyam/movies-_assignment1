import React, { Component } from 'react'
import { Text, View,StyleSheet } from 'react-native'
export default class FilmCard extends Component {
    render() {
        return (
            <View style={{width:'100%',height:125,backgroundColor:'#4d4d4d',flexDirection:'row',alignItems:'center',margin:5,borderRadius:15}}>
                    <View style={{width:75,height:75,backgroundColor:'#ffae00',margin:10,borderRadius:15,justifyContent:'center',alignItems:'center'}}>
                        <Image source={{uri:value.Poster}} style={{width:50,height:100}}/>
                    </View>
                    <View style={{width:360,height:75,}}>
                        <Text style={styles.text}>Title        : {this.props.Title}</Text>
                        <Text style={styles.text}>Year        : {this.props.Year}</Text>
                        <Text style={styles.text}>imdbID   : {this.props.imdbID}</Text>
                        <Text style={styles.text}>Type       : {this.props.Type}</Text>
                    </View>
            </View>
        )
    }
}
const styles=StyleSheet.create({
    text:{
        color:'#fff'

    }
})